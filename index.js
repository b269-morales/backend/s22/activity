help();

/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];
console.log('Current registered users:')
console.log(registeredUsers)

function checkRegisteredUsers(){
	console.log('Current registered users:');
	console.log(registeredUsers);
}

function help(){
	console.log('Commands List: ');
	console.log('You can also check the list of commands with: ');
	console.log('help()');
	console.log('=========================');
	console.log('checkRegisteredUsers()');
	console.log('register(" -input_name- ")');
	console.log('addFriend(" -input_name- ")');
	console.log('checkFriendsList()');
	console.log('howManyFriends()');
	console.log('deleteLatestFriend()');
	console.log('deleteSpecificFriend(" -input_name- ")');
	console.log('=========================');
	console.log('');
}

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/
    
    function register(newUser){
    	let newUserTrim = newUser.trim(); /*Ignores the spaces at the end of the command*/
    	let userRepeatCheck = registeredUsers.includes(newUserTrim);
    	
    	if (userRepeatCheck === true){
    		console.log('');
    		console.log('Registration failed. Username already exists!');
    		console.log('');
    		alert('Registration failed. Username already exists!')
    	} else {
    		registeredUsers.push(newUser);
    		console.log('');
    		console.log('Thank you for registering, ' + newUserTrim + "!");
    		console.log('');
    		console.log('Current registered users:');
    		console.log(registeredUsers);
    		console.log('');
    	}
    }

    


/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/

    function addFriend(newFriend){
    	let newFriendTrim = newFriend.trim();
    	let foundUser = registeredUsers.includes(newFriendTrim);

    	if (foundUser == true){
    		let userAlreadyInFriendsList = friendsList.includes(newFriendTrim);

    		if (userAlreadyInFriendsList == false){
    			friendsList.push(newFriendTrim);
    			console.log('');
    			console.log('You have added ' + newFriendTrim + " as a friend!");
    			console.log('');
    			console.log('Current friends list:');
    			console.log(friendsList);
    			console.log('');
    			alert('You have added ' + newFriendTrim + " as a friend!");
    		} else {
    			console.log('');
    			console.log('You tried to add ' + newFriendTrim + " as a friend");
    			console.log('but they are already in your friends list.');
    			console.log('');
    			console.log('Current friends list:');
    			console.log(friendsList);
    			console.log('');
    			alert('Cannot add ' + newFriendTrim + ' as a friend as they are already in your friends list.')
    		}



    	} else {
    		console.log('');
    		console.log('User not found.');
    		console.log('');
    		console.log('Please add from the current list of registered users:');
    		console.log(registeredUsers);
    		console.log('');
    		alert('User not found.');
    	}
    }

/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/

	function checkFriendsList(){

		if (friendsList.length > 0){
			for (let i = 0; i < friendsList.length; i++) {
				console.log(friendsList[i]);
			}
		} else {
			console.log('');
    		console.log('You currently have 0 friends. Add one first.');
    		console.log('');
    		console.log('Please add from the current list of registered users:');
    		console.log(registeredUsers);
    		console.log('');
    		alert('You currently have 0 friends. Add one first.');
		}
	}



/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/

	function howManyFriends(){
		if (friendsList.length == 0){
			console.log('');
			console.log('You currently have 0 friends. Add one first.');
			console.log('');
			console.log('Please use the addFriend command.');
			console.log('Add from the current list of registered users:');
			console.log(registeredUsers);
			console.log('');
			alert('You currently have 0 friends. Add one first.'); 

		} else{
			console.log('');
			console.log('You currently have '+ friendsList.length  +' friends.');
			console.log('');
			alert('You currently have '+ friendsList.length  +' friends.');
		}
	}


/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/

	function deleteLatestFriend(){
		if (friendsList.length == 0){
			console.log('');
			console.log('You currently have 0 friends. Add one first.');
			console.log('');
			console.log('Please use the addFriend command.');
			console.log('Add from the current list of registered users:');
			console.log(registeredUsers);
			console.log('');
			alert('You currently have 0 friends. Add one first.');

		} else{
			console.log('Deleted your latest friend, ' + friendsList.pop());
			console.log('');
			console.log('You current friends are:');
				console.log(friendsList);
				console.log('');
		}
	}

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/

	function deleteSpecificFriend(friendToBeDeleted){
		let friendToBeDeletedTrim = friendToBeDeleted.trim();
		let foundFriend = friendsList.includes(friendToBeDeletedTrim);
		let friendIndex = friendsList.indexOf(friendToBeDeletedTrim);

		if (friendsList.length == 0){
			console.log('');
			console.log('You currently have 0 friends. Add one first.');
			console.log('');
			console.log('Please use the addFriend command.');
			console.log('Add from the current list of registered users:');
			console.log(registeredUsers);
			console.log('');
			alert('You currently have 0 friends. Add one first.');

		} else{

			if (foundFriend == true){
				friendsList.splice(friendIndex, 1);
				console.log('Deleted ' + friendToBeDeletedTrim +' from your friends list.' );
				console.log('');
				console.log('You current friends are:');
				console.log(friendsList);
				console.log('');
			} 
			else {
				console.log('');
				console.log('Cannot delete ' + friendToBeDeletedTrim +' as they are not your friend.');
				console.log('');
				alert('Cannot delete ' + friendToBeDeletedTrim +' as they are not your friend.');
			}
		}
	}






